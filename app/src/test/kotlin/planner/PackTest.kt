package planner

import org.hamcrest.core.IsEqual.equalTo
import org.junit.Assert.assertThat
import org.junit.Test

class PackTest {

    private val pack = Pack(mutableListOf(PackItem(1001, 6200, 30, 9.6)))
    private val pack2 = Pack()
    private val config = PackItemConfig(maxQuantity = 100, maxWeight = 1000.0)

    @Test
    fun totalWeightTest() {
        assertThat(pack.totalWeight(), equalTo(288.0))
        pack.items.add(PackItem(2001, 7200, 50, 11.21))
        assertThat(pack.totalWeight(), equalTo(848.5))
        pack.items.add(PackItem(2001, 8200, 14, 22.20))
        assertThat(pack.totalWeight(), equalTo(1159.3))
    }

    @Test
    fun totalQuantityTest() {
        assertThat(pack2.totalQuantity(), equalTo(0))
        pack2.items.add(PackItem(1001, 6200, 30, 9.653))
        assertThat(pack2.totalQuantity(), equalTo(30))
        pack2.items.add(PackItem(2001, 7200, 50, 11.21))
        assertThat(pack2.totalQuantity(), equalTo(80))
        pack2.items.add(PackItem(2001, 8200, 14, 22.20))
        assertThat(pack2.totalQuantity(), equalTo(94))
    }

    @Test
    fun lengthTest() {
        assertThat(pack2.length(), equalTo(0))
        pack2.items.add(PackItem(1001, 6200, 30, 9.653))
        assertThat(pack2.length(), equalTo(6200))
        pack2.items.add(PackItem(2001, 7200, 50, 11.21))
        assertThat(pack2.length(), equalTo(7200))
        pack2.items.add(PackItem(2001, 8200, 14, 22.20))
        assertThat(pack2.length(), equalTo(8200))
    }
    @Test
    fun fillWithMaximumItemCountTest() {
        assertThat(pack2.totalQuantity(), equalTo(0))
        assertThat(pack2.totalWeight(), equalTo(0.0))
        assertThat(pack2.length(), equalTo(0))
        assertThat(pack2.fillWith(PackItem(1001, 6200, 30, 2.0), config), equalTo(PackItem(1001, 6200, 0, 2.0)))
        assertThat(pack2.totalQuantity(), equalTo(30))
        assertThat(pack2.totalWeight(), equalTo(60.0))
        assertThat(pack2.length(), equalTo(6200))
        assertThat(pack2.fillWith(PackItem(1002, 6200, 30, 0.8), config), equalTo(PackItem(1002, 6200, 0, 0.8)))
        assertThat(pack2.totalQuantity(), equalTo(60))
        assertThat(pack2.totalWeight(), equalTo(84.0))
        assertThat(pack2.length(), equalTo(6200))
        assertThat(pack2.fillWith(PackItem(2003, 7200, 50, 70.5), config), equalTo(PackItem(2003, 7200, 38, 70.5)))
        assertThat(pack2.totalQuantity(), equalTo(72))
        assertThat(pack2.totalWeight(), equalTo(930.0))
        assertThat(pack2.length(), equalTo(7200))

        assertThat(pack2.fillWith(PackItem(2004, 8200, 100, 0.6), config), equalTo(PackItem(2004, 8200, 72, 0.6)))
        assertThat(pack2.totalQuantity(), equalTo(100))
        assertThat(pack2.totalWeight(), equalTo(946.8))
        assertThat(pack2.length(), equalTo(8200))

        assertThat(
            pack2,
            equalTo(Pack(mutableListOf(PackItem(1001, 6200, 30, 2.0),
                PackItem(1002, 6200, 30, 0.8),
                PackItem(2003, 7200, 12, 70.5),
                PackItem(2004, 8200, 28, 0.6)),1))) //100 items in total

    }

    @Test
    fun fillWithMaximumWeightTest() {
        assertThat(pack2.totalQuantity(), equalTo(0))
        assertThat(pack2.totalWeight(), equalTo(0.0))
        assertThat(pack2.length(), equalTo(0))
        assertThat(pack2.fillWith(PackItem(1001, 7200, 30, 10.0), config), equalTo(PackItem(1001, 7200, 0, 10.0)))
        assertThat(pack2.totalQuantity(), equalTo(30))
        assertThat(pack2.totalWeight(), equalTo(300.0))
        assertThat(pack2.length(), equalTo(7200))
        assertThat(pack2.fillWith(PackItem(1002, 6200, 25, 30.0), config), equalTo(PackItem(1002, 6200, 2, 30.0)))
        assertThat(pack2.totalQuantity(), equalTo(53))
        assertThat(pack2.totalWeight(), equalTo(990.0))
        assertThat(pack2.length(), equalTo(7200))

        assertThat(pack2.fillWith(PackItem(1003, 6200, 3, 1.0), config), equalTo(PackItem(1003, 6200, 0, 1.0)))
        assertThat(pack2.totalQuantity(), equalTo(56))
        assertThat(pack2.totalWeight(), equalTo(993.0))
        assertThat(pack2.length(), equalTo(7200))

        assertThat(pack2.fillWith(PackItem(1004, 8200, 11, 1.0), config), equalTo(PackItem(1004, 8200, 4, 1.0)))
        assertThat(pack2.totalQuantity(), equalTo(63))
        assertThat(pack2.totalWeight(), equalTo(1000.0))
        assertThat(pack2.length(), equalTo(8200))

        //These are not added...
        assertThat(pack2.fillWith(PackItem(1003, 6200, 3, 30.0), config), equalTo(PackItem(1003, 6200, 3, 30.0)))
        assertThat(pack2.totalQuantity(), equalTo(63))
        assertThat(pack2.totalWeight(), equalTo(1000.0))
        assertThat(pack2.length(), equalTo(8200))
        assertThat(pack2.fillWith(PackItem(1004, 6200, 0, 0.0), config), equalTo(PackItem(1004, 6200, 0, 0.0)))
        assertThat(pack2.totalQuantity(), equalTo(63))
        assertThat(pack2.totalWeight(), equalTo(1000.0))
        assertThat(pack2.length(), equalTo(8200))
        assertThat(pack2.fillWith(PackItem(1004, 6200, -1, 100.0), config), equalTo(PackItem(1004, 6200, 0, 100.0)))
        assertThat(pack2.totalQuantity(), equalTo(63))
        assertThat(pack2.totalWeight(), equalTo(1000.0))
        assertThat(pack2.length(), equalTo(8200))
        assertThat(pack2.fillWith(PackItem(1004, 6200, 10, -100.0), config), equalTo(PackItem(1004, 6200, 0, -100.0)))
        assertThat(pack2.totalQuantity(), equalTo(63))
        assertThat(pack2.totalWeight(), equalTo(1000.0))
        assertThat(pack2.length(), equalTo(8200))
        assertThat(pack2.fillWith(PackItem(1004, 6200, -10, -100.0), config), equalTo(PackItem(1004, 6200, 0, -100.0)))
        assertThat(pack2.totalQuantity(), equalTo(63))
        assertThat(pack2.totalWeight(), equalTo(1000.0))
        assertThat(pack2.length(), equalTo(8200))

        assertThat(
            pack2,
            equalTo(Pack(mutableListOf(PackItem(1001, 7200, 30, 10.0),
                PackItem(1002, 6200, 23, 30.0),
                PackItem(1003, 6200, 3, 1.0),
                PackItem(1004, 8200, 7, 1.0)),1)))
    }
}