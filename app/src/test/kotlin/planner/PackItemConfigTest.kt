package planner

import org.junit.Assert.assertEquals
import org.junit.Test


class PackItemConfigTest {
    private val lineData = listOf("NATURAL", "40", "500.0")
    private val onlyTwoItems = listOf("SHORT_TO_LONG", "9.653")
    private val incorrectItems = listOf("LONG_TO_SHORT", "6200.0", "9.653")
    private val nonNumericItems = listOf("1001.0", "6200.0", "30", "9.653")

    @Test
    fun listDataConstructorTest() {
        assertEquals(PackItemConfig(lineData), PackItemConfig(SortOrder.NATURAL, 40, 500.0))
    }

    @Test(expected = NumberFormatException::class)
    fun onlyThreeItemsTest() {
        PackItemConfig(onlyTwoItems)
    }

    @Test(expected = NumberFormatException::class)
    fun incorrectNumericItemsTest() {
        PackItemConfig(incorrectItems)
    }

    @Test(expected = IllegalArgumentException::class)
    fun nonNumericItemsTest() {
        PackItemConfig(nonNumericItems)
    }
}