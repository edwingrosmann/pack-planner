package planner

import org.junit.Assert.assertEquals
import org.junit.Test

class PackItemTest {
    val lineData = listOf("1001", "6200", "30", "9.653")
    val onlyThreeItems = listOf("6200", "30", "9.653")
    val incorrectNumericItems = listOf("1001.0", "6200.0", "30", "9.653")
    val nonNumericItems = listOf("Bibbidi", "Babbidi", "Boo", "9.653")

    @Test
    fun listDataConstructorTest() {
        assertEquals(PackItem(lineData), PackItem(1001, 6200, 30, 9.653))
    }

    @Test(expected = NumberFormatException::class)
    fun onlyThreeItemsTest() {
        PackItem(onlyThreeItems)
    }

    @Test(expected = IllegalArgumentException::class)
    fun incorrectNumericItemsTest() {
        PackItem(incorrectNumericItems)
    }

    @Test(expected = NumberFormatException::class)
    fun nonNumericItemsTest() {
        PackItem(nonNumericItems)
    }
}
