package planner

import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.Before
import org.junit.Test

class PackPlannerKtTest {

    @Before
    fun setup() {
        mockkStatic("planner.PackPlannerKt")
        packs = mutableListOf()
    }

    @Test
    fun neutralSortTest() {
        scanner = mockk {
            every { nextLine() } returnsMany listOf(
                "NATURAL,40,500.0",
                "1001,6200,30,9.653",
                "2001,7200,50,11.21",
                ""
            )
        }

        main()

        assertThat(
            config,
            equalTo(PackItemConfig(SortOrder.NATURAL, 40, 500.0))
        )
        assertThat(
            packetInputData,
            equalTo(
                listOf(
                    PackItem(1001, 6200, 30, 9.653),
                    PackItem(2001, 7200, 50, 11.21)
                )
            )
        )
        assertThat(
            packs,
            equalTo(
                listOf(
                    Pack(
                        mutableListOf(
                            PackItem(1001, 6200, 30, 9.653),
                            PackItem(2001, 7200, 10, 11.21)
                        ), 1
                    ),
                    Pack(mutableListOf(PackItem(2001, 7200, 40, 11.21)), 2)
                )
            )
        )

        assertThat(
            report,
            equalTo(
                "Pack Number: 1\n" +
                        "1001,6200,30,9.653\n" +
                        "2001,7200,10,11.21\n" +
                        "Pack Length: 7200, Pack Weight: 401.69\n" +
                        "\n" +
                        "Pack Number: 2\n" +
                        "2001,7200,40,11.21\n" +
                        "Pack Length: 7200, Pack Weight: 448.4\n" +
                        "\n"
            )
        )
    }

    @Test
    fun longToShortTest() {
        scanner = mockk {
            every { nextLine() } returnsMany listOf(
                "LONG_TO_SHORT,40,500.0",
                "1001,6200,30,9.653",
                "3001,8200,50,11.21",
                "2001,7200,50,11.21",
                ""
            )
        }

        main()
        assertThat(
            config,
            CoreMatchers.equalTo(PackItemConfig(SortOrder.LONG_TO_SHORT, 40, 500.0))
        )
        assertThat(
            packetInputData,
            equalTo(
                listOf(
                    PackItem(3001, 8200, 50, 11.21),
                    PackItem(2001, 7200, 50, 11.21),
                    PackItem(1001, 6200, 30, 9.653)
                )
            )
        )
        assertThat(
            packs,
            equalTo(
                listOf(
                    Pack(mutableListOf(PackItem(3001, 8200, 40, 11.21)), 1),
                    Pack(
                        mutableListOf(
                            PackItem(3001, 8200, 10, 11.21),
                            PackItem(2001, 7200, 30, 11.21)
                        ), 2
                    ),
                    Pack(
                        mutableListOf(
                            PackItem(2001, 7200, 20, 11.21),
                            PackItem(1001, 6200, 20, 9.653)
                        ), 3
                    ),
                    Pack(mutableListOf(PackItem(1001, 6200, 10, 9.653)), 4)
                )
            )
        )
        assertThat(
            report,
            equalTo(
                "Pack Number: 1\n" +
                        "3001,8200,40,11.21\n" +
                        "Pack Length: 8200, Pack Weight: 448.4\n" +
                        "\n" +
                        "Pack Number: 2\n" +
                        "3001,8200,10,11.21\n" +
                        "2001,7200,30,11.21\n" +
                        "Pack Length: 8200, Pack Weight: 448.4\n" +
                        "\n" +
                        "Pack Number: 3\n" +
                        "2001,7200,20,11.21\n" +
                        "1001,6200,20,9.653\n" +
                        "Pack Length: 7200, Pack Weight: 417.26\n" +
                        "\n" +
                        "Pack Number: 4\n" +
                        "1001,6200,10,9.653\n" +
                        "Pack Length: 6200, Pack Weight: 96.53\n" +
                        "\n"
            )
        )
    }

    @Test
    fun shortToLongTest() {
        scanner = mockk {
            every { nextLine() } returnsMany listOf(
                "SHORT_TO_LONG,40,500.0",
                "1001,6200,30,9.653",
                "3001,8200,50,11.21",
                "2001,7200,50,11.21",
                ""
            )
        }

        main()
        assertThat(
            config,
            CoreMatchers.equalTo(PackItemConfig(SortOrder.SHORT_TO_LONG, 40, 500.0))
        )
        assertThat(
            packetInputData,
            equalTo(
                listOf(
                    PackItem(1001, 6200, 30, 9.653),
                    PackItem(2001, 7200, 50, 11.21),
                    PackItem(3001, 8200, 50, 11.21)
                )
            )
        )
        assertThat(
            packs,
            equalTo(
                listOf(
                    Pack(mutableListOf(PackItem(1001, 6200, 30, 9.653), PackItem(2001, 7200, 10, 11.21)), 1),
                    Pack(
                        mutableListOf(
                            PackItem(2001, 7200, 40, 11.21)
                        ), 2
                    ),
                    Pack(
                        mutableListOf(
                            PackItem(3001, 8200, 40, 11.21)
                        ), 3
                    ),
                    Pack(mutableListOf(PackItem(3001, 8200, 10, 11.21)), 4)
                )
            )
        )
        assertThat(
            report,
            equalTo(
                "Pack Number: 1\n" +
                        "1001,6200,30,9.653\n" +
                        "2001,7200,10,11.21\n" +
                        "Pack Length: 7200, Pack Weight: 401.69\n" +
                        "\n" +
                        "Pack Number: 2\n" +
                        "2001,7200,40,11.21\n" +
                        "Pack Length: 7200, Pack Weight: 448.4\n" +
                        "\n" +
                        "Pack Number: 3\n" +
                        "3001,8200,40,11.21\n" +
                        "Pack Length: 8200, Pack Weight: 448.4\n" +
                        "\n" +
                        "Pack Number: 4\n" +
                        "3001,8200,10,11.21\n" +
                        "Pack Length: 8200, Pack Weight: 112.1\n" +
                        "\n"
            )
        )
    }

    @Test
    fun incorrectConfigTest() {
        scanner = mockk {
            every { nextLine() } returnsMany listOf(
                "SHORT_TO_LONG,40,500.0,7.0",
                "1001,6200,30,9.653",
                "3001,8200,50,11.21",
                "2001,7200,50,11.21",
                ""
            )
        }

        main()

        assertThat(
            report,
            equalTo("""An error has happened: The first Line MUST have format "[Sort order],[max pieces per pack],[max weight per pack]" e.g. "NATURAL,40,500.0" """)
        )
    }

    @Test
    fun incorrectLineItemTest() {
        scanner = mockk {
            every { nextLine() } returnsMany listOf(
                "SHORT_TO_LONG,40,500.0",
                "1001,6200,30,9.653,100.0",
                ""
            )
        }

        main()

        assertThat(
            report,
            equalTo("""An error has happened: [1001, 6200, 30, 9.653, 100.0]: The Items lines MUST have format "[item id],[item length],[item quantity],[piece weight]" e.g. "1001,6200,30,9.653" """)
        )
    }
}