package planner

import java.text.DecimalFormat
import java.util.*

lateinit var config: PackItemConfig
lateinit var packetInputData: List<PackItem>
lateinit var report: String
var scanner = Scanner(System.`in`)
var packs = mutableListOf<Pack>()
val df = DecimalFormat("#.###")

/**
 * This Pack Planner parses the input-data from an input-stream of choice (see README.md for details) </br>
 * The resulting sorted list of PackItems is then sliced and diced into Packs; taking into account the maximum item-count and -weight </br>
 * Finally this List of Packs is crafted into a report which is printed to the standard output (see README.md for details) </br>
 */
fun main() {
    try {
        parseInputData()
        assemblePacks()
        createReport()
    } catch (e: Exception) {
        printErrorReport(e)
    }
}

/**
 * Parses the input data into the ```config``` and ```packetInputData```
 */
private fun parseInputData() {
    config = packItemConfig()
    packetInputData = packetInputData()
    sortItems()
}

/**
 * Takes the sorted  packet input data and distributes it over packs as per the maximum weight and -quantity requirements.
 * The result is a List of Packs
 */
fun assemblePacks() {

    var pack = Pack(packNumber = 1)

    packetInputData.forEach {
        var item: PackItem = it

        //When the returned item is NOT empty,
        // the pack has been filled to max capacity and is to be stored in the result...
        do {
            item = pack.fillWith(item, config)
            if (!item.isEmpty()) {
                packs.add(pack)
                pack = Pack(packNumber = pack.packNumber + 1)
            }
        } while (!item.isEmpty())
    }
    packs.add(pack)
}

/**
 * Takes the List of Packs and creates a report for them.
 * The report is then printed to the standard output
 */
fun createReport() {
    val sb = StringBuilder()

    packs.forEach { pack ->
        sb.appendln("Pack Number: ${pack.packNumber}")
        pack.items.forEach {
            sb.appendln("${it.itemId},${it.length},${it.quantity},${df.format(it.weight)}")
        }
        sb.appendln("Pack Length: ${pack.length()}, Pack Weight: ${df.format(pack.totalWeight())}")
        sb.appendln()
    }
    report = sb.toString()

    println(report)
}

/**
 * The error will become the report
 */
private fun printErrorReport(e: Exception) {
    report = "An error has happened: ${e.message}"
    println(report)
}

/**
 * Sort the packet input data as per the sort-order
 */
private fun sortItems() {
    when (config.sortOrder) {
        SortOrder.NATURAL -> {
        }
        SortOrder.SHORT_TO_LONG -> packetInputData = packetInputData.sortedBy { it.length }
        SortOrder.LONG_TO_SHORT -> packetInputData = packetInputData.sortedByDescending { it.length }
    }
}

/**
 * Parses input lines 2 and beyond into Packet-Items
 * e.g:
 * ```
 * 1001,6200,30,9.653
 * 2001,7200,50,11.21
 * ```
 * @return the parsed data
 */
private fun packetInputData(): MutableList<PackItem> {
    val packetInputData = mutableListOf<PackItem>()
    while (processLine(scanner.nextLine(), packetInputData).isNotBlank()) {
    }
    return packetInputData
}

/**
 * Tries to parse the line-string into a PackItem. When successful, this Pack-item is added to the list.
 * @throws  NoSuchElementException when the line is not a CSV-string with four elements
 * @return the parsed line
 */
private fun processLine(line: String, packetInputData: MutableList<PackItem>): String {

    //Only process when the line contains data...
    if (line.isNotBlank()) {

        //Try to parse the line; it must - for starters - contain four elements; if not throw an exception...
        line.asList().let {
            if (it.hasValidSize(4)) {
                packetInputData.add(PackItem(it))
            } else {
                throw  NoSuchElementException("""$it: The Items lines MUST have format "[item id],[item length],[item quantity],[piece weight]" e.g. "1001,6200,30,9.653" """)
            }
        }
    }
    return line
}

/**
 * Parses the first input line into a Config-Object
 * e.g:
 * ```
 * NATURAL,40,500.0
 * ```
 * @return The PacketIem Config
 * @throws NoSuchElementException in case the config-line  could not be parsed
 */
fun packItemConfig(): PackItemConfig {
    with(scanner.nextLine().asList()) {
        if (hasValidSize(3)) return PackItemConfig(this)
    }
    throw  NoSuchElementException("""The first Line MUST have format "[Sort order],[max pieces per pack],[max weight per pack]" e.g. "NATURAL,40,500.0" """)
}

private fun <E> List<E>.hasValidSize(requiredSize: Int): Boolean {
    return size == requiredSize
}

private fun String.asList() = this.split(',')
