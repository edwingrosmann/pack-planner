package planner

import kotlin.math.max
import kotlin.math.min

/**
 * The input data sort-options
 */
enum class SortOrder { NATURAL, SHORT_TO_LONG, LONG_TO_SHORT }

/**
 * The First line of the Input data
 */
data class PackItemConfig(val sortOrder: SortOrder = SortOrder.NATURAL, val maxQuantity: Int = 0, val maxWeight: Double = 0.0) {
    constructor(l: List<String>) : this(SortOrder.valueOf(l[0]), l[1].toInt(), l[2].toDouble())
}

/**
 * Lines 2 and up of the input-data
 */
data class PackItem(val itemId: Int, val length: Int, val quantity: Int, val weight: Double) {
    constructor(data: List<String>) : this(data[0].toInt(), data[1].toInt(), data[2].toInt(), data[3].toDouble())

    fun isEmpty() = weight <= 0 || quantity <= 0
}

/**
 * A collection of PackItems;
 * It can take Packitem and return the portion that exceeds quantity- and/or weight restrictions;
 * this remainder can then be used to stack other packs.
 */
data class Pack(val items: MutableList<PackItem> = mutableListOf(), val packNumber: Int = 1) {
    fun length(): Int {
        return items.fold(0) { acc, packItem -> max(acc, packItem.length) }
    }

    fun totalWeight(): Double {
        return items.fold(0.0) { acc, packItem -> acc + packItem.weight * packItem.quantity }
    }

    fun totalQuantity(): Int {
        return items.fold(0) { acc, packItem -> acc + packItem.quantity }
    }

    /**
     * Take all the content of the packItem that will fit into the pack, return the remainder
     */
    fun fillWith(item: PackItem, config: PackItemConfig): PackItem {

        //Consider only items that have quantity AND weight...
        if (item.isEmpty()) return PackItem(item.itemId, item.length, 0, item.weight)


        //The quantity that can be added, not exceeding the weight limitations...
        val maxTakeQuantityByWeight = ((config.maxWeight - totalWeight()) / item.weight).toInt()

        //The quantity that could still be added, ignoring weight limitations...
        val remainingQuantity = config.maxQuantity - totalQuantity()

        //The maximum quantity within the bound of both maximum-weight and maximum-overall-quantity...
        val maxTakeQuantity: Int = min(maxTakeQuantityByWeight, remainingQuantity)

        //Only continue when the pack is not already filled to a max ...
        if (maxTakeQuantity <= 0) return item

        //Take the whole lot...
        if (maxTakeQuantity > item.quantity) {
            items.add(item)
            return PackItem(item.itemId, item.length, 0, item.weight)
        }
        //Take all the quantity that is allowed; return the items that could not be fitted...
        else {
            val returnedQuantity = item.quantity - maxTakeQuantity
            items.add(PackItem(item.itemId, item.length, maxTakeQuantity, item.weight))
            return PackItem(item.itemId, item.length, returnedQuantity, item.weight)
        }
    }
}